api = 2
core = 7.x

; Build Kit 7.x-2.x HEAD
includes[] = http://drupalcode.org/project/buildkit.git/blob_plain/refs/heads/7.x-2.x:/drupal-org.make

;--------------------
; Build Kit overrides
;--------------------

projects[tao][subdir] = contrib

projects[rubik][subdir] = contrib

;--------------------
; Additional Contrib
;--------------------

projects[boxes][subdir] = contrib
projects[boxes][version] = 1.0-beta7

projects[markdown][subdir] = contrib
projects[markdown][version] = 1.0

; Creates invalid HTML with preformatted blocks.
; http://drupal.org/node/684554
projects[markdown][patch][684554] = http://drupal.org/files/issues/markdown-684554.patch

projects[sms_registration][type] = module
projects[sms_registration][subdir] = contrib
projects[sms_registration][download][type] = git
projects[sms_registration][download][url] = http://git.drupal.org/sandbox/christianchristensen/1489028.git
projects[sms_registration][download][branch] = 7.x-1.x
projects[sms_registration][version] = 1.x-dev

projects[sms_twilio][type] = module
projects[sms_twilio][subdir] = contrib
projects[sms_twilio][download][type] = git
projects[sms_twilio][download][url] = http://git.drupal.org/project/sms_twilio.git
projects[sms_twilio][download][revision] = daf42450f9b68dd68db1e86cd4d09e11185f9d70
projects[sms_twilio][version] = 1.x-dev
projects[sms_twilio][patch][] = http://drupal.org/files/sms_twilio-drush-1489854-2-D7.patch
projects[sms_twilio][patch][] = http://drupal.org/files/sms_twilio-library-update-1225966-1-D7.patch

projects[smsframework][type] = module
projects[smsframework][subdir] = contrib
projects[smsframework][download][type] = git
projects[smsframework][download][url] = http://git.drupal.org/sandbox/christianchristensen/1489070.git
projects[smsframework][download][branch] = 7.x-1.x-patched
; projects[smsframework][download][url] = http://git.drupal.org/project/smsframework.git
; projects[smsframework][download][revision] = aeb577dc718fb78257371867add960b34f872d6e
; projects[smsframework][version] = 1.x-dev
; "fix" for sms_carriers missing in 7.x port
; projects[smsframework][patch][] = http://drupal.org/files/smsframework-sms_carriers-1421294-1.patch
; Update sms_user to 7.x dbtng compatibility
; projects[smsframework][patch][] = http://drupal.org/files/smsframework-sms_user-upgrade-1247538-D7-24.patch
; projects[smsframework][patch][] = http://drupal.org/files/smsframework-default-check-array-notice-1477762-D7-2.patch

projects[token][subdir] = contrib
projects[token][version] = 1.0-rc1

;--------------------
; Libraries
;--------------------

libraries[twilio][download][type] = git
libraries[twilio][download][url] = https://github.com/twilio/twilio-php.git
; tag: 2.0.8
libraries[twilio][download][revision] = a8ee59fb46e5be7017b0fc90c3b4a56dbce2c26d
libraries[twilio][destination] = libraries

;--------------------
; Custom
;--------------------


;--------------------
; Development
;--------------------

projects[coder][subdir] = contrib
projects[coder][version] = 1.0
