## A simple SMS, configured with Twilio, Drupal distibution.

### Build

Install `drush`[1] on your system and run the following command:

    $ drush make --working-copy simple_sms_distro.make public_html

### Notes

This project contains a distro features module, `simple_sms_config`.

At this time, Simple SMS does not attempt to maintain backwards compatability between updates.

<h3>Description</h3>
Simple SMS installation profile for setting up a drupal site with SMS registration and communication.


<h3>Example</h3>
http://dev.dta.gotpantheon.com/
To register for the site: Text: (something witty) to 972-360-9041.


<h3>Info</h3>
An example Drupal 7 makefile/install profile with Twilio SMS configs baked in. This recipes (and patches to pull D7 fixes) existing modules, such as:
<ul>
  <li><a href="http://drupal.org/project/smsframework">SMS Framework</a></li>
  <li><a href="http://drupal.org/project/sms_twilio">Twilio SMS integration</a></li>
  <li><a href="http://drupal.org/sandbox/christianchristensen/1489028">SMS Registration (sandbox)</a></li>
</ul>


<h3>Further</h3>
Since this project sets up SMS framework which provides send methods to the Messaging and Notifications module pair - it would be easy to pull this feature configuration and re-use it on other distributions (such as OpenPublic).


<h4>Quick How To</h3>
<code>
drush make http://goo.gl/vc7Lq public_html
</code>


### Thanks

Built on top of [Build Kit](http://drupal.org/project/buildkit)
and [simple](https://github.com/sprice/simple)

[1] http://drupal.org/project/drush

