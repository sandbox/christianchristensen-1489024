<?php
/**
 * @file
 * simple_sms_defaults.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function simple_sms_defaults_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer smsframework
  $permissions['administer smsframework'] = array(
    'name' => 'administer smsframework',
    'roles' => array(),
  );

  // Exported permission: edit own sms number
  $permissions['edit own sms number'] = array(
    'name' => 'edit own sms number',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'sms_user',
  );

  // Exported permission: receive sms
  $permissions['receive sms'] = array(
    'name' => 'receive sms',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'sms_user',
  );

  return $permissions;
}
